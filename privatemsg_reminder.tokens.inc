<?php

/**
 * @file
 * Token callbacks for the Privatemsg Reminder module.
 */

/**
 * Implements hook_token_info().
 */
function privatemsg_reminder_token_info() {
  $info = array(
    'types' => array(
      'privatemsg_unread_messages' => array(
        'name' => t('Unread private messages'),
        'description' => t('Tokens related to the Privatemsg module.'),
        'needs-data' => 'privatemsg_unread_messages',
      ),
    ),
    'tokens' => array(
      'privatemsg_unread_messages' => array(
        'recipient' => array(
          'name' => t('Recipient'),
          'description' => t('The recipient of the messages.'),
          'type' => 'user',
        ),
        'messages' => array(
          'name' => t('Messages'),
          'description' => t('The array of formatted messages to send.'),
          'type' => 'array',
        ),
        'url' => array(
          'name' => t('URL'),
          'description' => t('URL that points to all messages.'),
        ),
      ),
    ),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function privatemsg_reminder_tokens($type, array $tokens, array $data = array(), array $options = array()) {
  $return = array();

  if ($type == 'privatemsg_unread_messages' && !empty($data['privatemsg_unread_messages'])) {
    $recipient = $data['privatemsg_recipient'];
    $messages = $data['privatemsg_unread_messages'];
    $rendered_messages = _privatemsg_reminder_render_messages($messages);

    // Elaboration replacements.
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'messages:first':
          $return[$original] = reset($rendered_messages);
          break;

        case 'messages:last':
          $return[$original] = end($rendered_messages);
          break;

        case 'messages:count':
          $return[$original] = count($messages);
          break;

        case 'messages:keys':
          $return[$original] = token_render_array(array_keys($messages), $options);
          break;

        case 'messages:reversed':
          $reversed = array_reverse(array_keys($messages), TRUE);
          $return[$original] = token_render_array($reversed, $options);
          break;

        case 'messages':
        case 'messages:join':
          $return[$original] = token_render_array($rendered_messages, array('join' => '', 'sanitize' => FALSE) + $options);
          break;


        case 'url':
          $return[$original] = url('messages/list', array('absolute' => TRUE));
          break;
      }
    }

    if ($recipient_tokens = token_find_with_prefix($tokens, 'recipient')) {
      $return += token_generate('user', $recipient_tokens, array('user' => $recipient), $options);
    }
  }

  return $return;
}

/**
 * Render private messages.
 *
 * @param array $messages
 *
 * @return array
 */
function _privatemsg_reminder_render_messages($messages = array()) {
  $templates = privatemsg_reminder_mail_edit_text('reminder', LANGUAGE_NONE);
  $template = variable_get('privatemsg_reminder_message_body', $templates['privatemsg_reminder_message_body']);

  $rendered_messages = array();
  foreach ($messages as $message) {
    $rendered_messages[] = token_replace($template, array('privatemsg_message' => $message));
  }

  return $rendered_messages;
}
