<?php

/**
 * @file
 * Integration with Mail Editor module.
 */

/**
 * Implements hook_mailkeys().
 */
function privatemsg_reminder_mailkeys() {
  return array(
    'reminder' => t('Privatemsg reminder of unread messages'),
  );
}

/**
 * Implements hook_mail_edit_text().
 */
function privatemsg_reminder_mail_edit_text($mailkey, $language) {
  switch ($mailkey) {
    case 'reminder':
      return array(
        'subject' => t('[privatemsg_unread_messages:recipient], you have [privatemsg_unread_messages:messages:count] unread messages.'),

        'body' => t('Hi [privatemsg_unread_messages:recipient],
This is an automatic reminder from the site [site:name]. You have [privatemsg_unread_messages:messages:count] unread messages.
[privatemsg_unread_messages:messages]
To read your messages, follow this link: [privatemsg_unread_messages:url]'),

        'privatemsg_reminder_message_body' => 'Author: [privatemsg_message:author]
Subject: [privatemsg_message:subject]
Body: [privatemsg_message:body]
url: [privatemsg_message:url]',
      );

    default :
      return array();
  }
}

/**
 * Implements hook_mail_edit_token_types().
 */
function privatemsg_reminder_mail_edit_token_types($mailkey) {
  return array('privatemsg_unread_messages');
}

/**
 * Implements hook_mail_edit_form_extra().
 *
 * Customize Mail Editor's edit template page.
 */
function privatemsg_reminder_mail_edit_form_extra(&$form, &$form_state, $mailkey, $template) {
  if ($mailkey == 'reminder') {
    $message_body = $template['privatemsg_reminder_message_body'];

    $form['mail']['privatemsg_reminder_message_body'] = array(
      '#title' => t('Message'),
      '#type' => 'textarea',
      '#default_value' => variable_get('privatemsg_reminder_message_body', $message_body),
      '#rows' => 5,
    );

    $form['mail']['tokens_for_msg'] = array(
      '#type' => 'fieldset',
      '#title' => t('Tokens for message'),
      '#weight' => 6,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'tokens' => array(
        '#theme' => 'token_tree',
        '#token_types' => array('privatemsg_message'),
        '#global_types' => TRUE,
        '#click_insert' => TRUE,
      ),
    );

    $form['op']['#submit'][] = 'privatemsg_reminder_mail_edit_form_extra_submit';
  }
}

/**
 * Submit handler to save Mail Editor's edit form.
 */
function privatemsg_reminder_mail_edit_form_extra_submit(array $form, array &$state) {
  $values = $state['values'];
  $message_template = $values['privatemsg_reminder_message_body'];
  variable_set('privatemsg_reminder_message_body', $message_template);
}