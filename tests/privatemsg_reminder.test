<?php

class PrivatemsgReminderTestCase extends PrivatemsgBaseTestCase {
  /**
   * Implements setUp().
   */
  protected function setUp() {
    parent::setUp('privatemsg', 'privatemsg_reminder', 'mail_edit', 'elysia_cron');

    $this->author = $this->drupalCreateUser(array('write privatemsg', 'read privatemsg'));
    $this->recipient1 = $this->drupalCreateUser(array('write privatemsg', 'read privatemsg'));
    $this->recipient2 = $this->drupalCreateUser(array('write privatemsg', 'read privatemsg'));
    $this->recipient3 = $this->drupalCreateUser(array('write privatemsg', 'read privatemsg'));

    $this->drupalLogin($this->author);

    // Send a message to two users.
    $message = array(
      'recipient' => $this->recipient1->name . ', ' . $this->recipient2->name,
      'subject' => $this->randomName(),
      'body[value]' => $this->randomName(20),
    );
    $this->drupalPost('messages/new', $message, t('Send message'));

    // And another message for same user.
    $message = array(
      'recipient' => $this->recipient1->name,
      'subject' => $this->randomName(),
      'body[value]' => $this->randomName(20),
    );
    $this->drupalPost('messages/new', $message, t('Send message'));

    // It should be 2 messages for recipient1 and 1 message for recipient2.
  }
}

/**
 * Class PrivatemsgReminderGetUsersTestCase
 *
 * @see: privatemsg_reminder_get_users_with_unread_messages()
 */
class PrivatemsgReminderGetUsersTestCase extends PrivatemsgReminderTestCase {
  public static function getInfo() {
    return array(
      'name' => t('Get users with unread messages'),
      'description' => t('Test getting users with unread messages.'),
      'group' => t('Privatemsg Reminder'),
    );
  }

  /**
   * Test getting users with unread messages.
   */
  function testGetUsersWithUnreadMessages() {
    // Should be two users with unread messages.
    $uids = privatemsg_reminder_get_users_with_unread_messages();
    $this->assertEqual(count($uids), 2, t('Returns right count of users with unread messages.'), 'privatemsg_reminder_get_users_with_unread_messages()');

    // Check returned users.
    foreach ($uids as $uid) {
      if (!in_array($uid, array($this->recipient1->uid, $this->recipient2->uid))) {
        $this->fail(t('Returns wrong users'), 'privatemsg_reminder_get_users_with_unread_messages()');
      }
    }
  }
}

/**
 * Class PrivatemsgReminderGetUnreadMessagesTestCase
 *
 * @see: privatemsg_reminder_get_unread_messages()
 */
class PrivatemsgReminderGetUnreadMessagesTestCase extends PrivatemsgReminderTestCase {
  public static function getInfo() {
    return array(
      'name' => t('Get unread messages'),
      'description' => t('Test getting unread messages from user.'),
      'group' => t('Privatemsg Reminder'),
    );
  }

  protected function setUp() {
    parent::setUp();

    // Add already read message.
    $subject = $this->randomName(20);
    $body = $this->randomName(50);
    $response = privatemsg_new_thread(array($this->recipient3), $subject, $body, array('author' => $this->author));

    $this->drupalLogout();
    $this->drupalLogin($this->recipient3);
    $this->drupalGet('messages/view/' . $response['message']->thread_id);

    // Login as author again.
    $this->drupalLogin($this->author);
  }

  /**
   * Test getting unread messages.
   */
  function testGetUnreadMessages() {
    // Should be two users with unread messages.
    $messages1 = privatemsg_reminder_get_unread_messages($this->recipient1);
    $messages2 = privatemsg_reminder_get_unread_messages($this->recipient2);
    $messages3 = privatemsg_reminder_get_unread_messages($this->recipient3);

    $this->assertEqual(count($messages1), 2, t('User 1 should have 2 unread messages.'), 'privatemsg_reminder_get_unread_messages()');
    $this->assertEqual(count($messages2), 1, t('User 2 should have 1 unread messages.'), 'privatemsg_reminder_get_unread_messages()');
    $this->assertEqual(count($messages3), 0, t('User 3 should have 0 unread messages.'), 'privatemsg_reminder_get_unread_messages()');
  }
}

/**
 * Class PrivatemsgReminderFillQueue
 *
 * @see: privatemsg_reminder_fill_queue()
 */
class PrivatemsgReminderFillQueue extends PrivatemsgReminderTestCase {
  public static function getInfo() {
    return array(
      'name' => t('Fill up drupal queue'),
      'description' => t('Get all users with unread messages and put then to the queue.'),
      'group' => t('Privatemsg Reminder'),
    );
  }

  protected function setUp() {
    parent::setUp();

    $this->cronRun(); // It should calls privatemsg_reminder_fill_queue()
  }

   /**
   * Test filling up drupal queue.
   */
  function testFillQueue() {
    $uids = db_select('queue', 'q')
      ->fields('q', array('data'))
      ->condition('q.name', 'privatemsg_reminder')
      ->execute()->fetchCol();

    // Should be two users in the queue.
    $this->assertEqual(count($uids), 2, t('Two users with unread messages in the queue.'), 'privatemsg_reminder_fill_queue()');

    // Users are right.
    foreach ($uids as $uid) {
      if (!in_array(unserialize($uid), array($this->recipient1->uid, $this->recipient2->uid))) {
        $this->fail(t('Wrong users in the queue'), 'privatemsg_reminder_fill_queue()');
      }
    }
  }
}